package com.compasso.coneboard.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.compasso.coneboard.model.Cone;

@RepositoryRestResource(collectionResourceRel = "cone", path = "cone")
public interface ConeRepository extends JpaRepository<Cone, Integer> {
}