package com.compasso.coneboard.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.compasso.coneboard.model.Conisse;

@RepositoryRestResource(collectionResourceRel = "conisse", path = "conisse")
public interface ConisseRepository extends JpaRepository<Conisse, Integer> {
}