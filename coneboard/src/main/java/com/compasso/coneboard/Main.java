package com.compasso.coneboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
@EnableAutoConfiguration
@Configuration
public class Main implements WebMvcConfigurer   {

		public static void main(String[] args) {
			SpringApplication.run(Main.class, args);
		}

	
}